﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using App2.iOS.Renderers;
using App2.Widgets;
using Foundation;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomEditor), typeof(CustomEditorRenderer))]
namespace App2.iOS.Renderers
{
    public class CustomEditorRenderer : EditorRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);

            if (Control == null)
                return;

            var uiLabels = GetViewsOfType<UILabel>(this);
            foreach (var item in uiLabels)
            {
                //item.RemoveConstraints(item.Constraints);
                //item.PreferredMaxLayoutWidth = 50f;
                //item.AdjustsFontSizeToFitWidth = true;
                //item.LineBreakMode = UILineBreakMode.CharacterWrap;
                //item.TextColor = UIColor.Red;
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
        }



        public IEnumerable<T> GetViewsOfType<T>(UIView uiView)
        {
            foreach (var item in uiView.Subviews)
            {
                if (item is T t)
                {
                    yield return t;

                }
                if (item.Subviews.Any())
                {
                    foreach (var item2 in GetViewsOfType<T>(item))
                    {
                        yield return item2;
                    };
                }
            }
        }
    }
}